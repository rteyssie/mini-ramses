This namelist performs a cooling halo simulation with dark matter and gas.
Gas cooling and star formation are included. It features a dynamo.
You need an ascii file for the initial conditions. You can download one from
tigress by executing in the command line the following script:
> utils/scripts/load_nfw_ic.sh
To compile the code with MPI, type in the command line:
> make NDIM=3 MPI=1 MHD=1 UNITS=merger PATCH=../patch/init/halo HYDRO=1 GRAV=1 NVAR=7

&RUN_PARAMS
cosmo=.false.
pic=.true.
poisson=.true.
hydro=.true.
nrestart=0
nsubcycle=1,1,2
ncontrol=1
verbose=.false.
/

&OUTPUT_PARAMS
delta_tout=0.05
tend=1.0
bkp_time_hrs=1
bkp_modulo=3
/

&INIT_PARAMS
filetype='ascii'
initfile(1)='nfw'
ic_scale_m=0.85
/

&AMR_PARAMS
levelmin=7
levelmax=13
ngridmax=2000000
npartmax=4000000
nexpand=1,
boxlen=140.
/

&POISSON_PARAMS
epsilon=1.d-4
/

&HYDRO_PARAMS
gamma=1.6666667
courant_factor=0.5
slope_type=1
T2_fix=1d4
dual_energy=0.2
entropy=.true.
scheme='muscl'
riemann='hlld'
riemann2d='hlld'
/

&COOLING_PARAMS
cooling=.true.
metal=.true.
haardt_madau=.true.
self_shielding=.true.
z_ave=1.0
z_reion=10
/

&STAR_PARAMS
star=.true.
nstarmax=2000000
n_star=100.
eps_star=0.1
m_star=1
/

&FEEDBACK_PARAMS
mechanical_feedback=.true.
thermal_feedback=.true.
/

&REFINE_PARAMS
m_refine=10*8.,
interpol_var=1
interpol_type=0
mass_sph=0.005
/

&HALO_PARAMS
v_200=35.
concentration=10
baryon_fraction=0.15
lambda=0.04
halo_eps=0.01
halo_nmin=1d-6
halo_tmin=1d5
B_s=1d-6
mag_topology='dipole'
/
